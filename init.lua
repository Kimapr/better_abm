local vmanips={}
local active_range=4*16
local ratio=1/3
local abmtimer=0
local chaosmap
local floor=math.floor
local ceil=math.ceil
local rand=math.random

local abms={}
minetest.registered_better_abms=abms

function minetest.register_abm(def)
	assert(type(def.action)=="function","Required field 'action' of type function")
	abms[#abms+1]=def
end

minetest.register_node("better_abm:test0",{
	tiles={"unknown_item.png"}
})

minetest.register_node("better_abm:test1",{
	tiles={"unknown_node.png"}
})

minetest.register_abm({
	interval=1,
	chance=1,
	nodenames={"better_abm:test0","better_abm:test1"},
	action=function(pos,node)
		minetest.set_node(pos,{name="better_abm:test1"})
		minetest.after(0.5,minetest.set_node,pos,{name="better_abm:test0"})
		--[[if node.name=="better_abm:test0" then
			minetest.set_node(pos,{name="better_abm:test1"})
		else
			minetest.set_node(pos,{name="better_abm:test0"})
		end]]
	end
})

local function gen_chaosmap()
	chaosmap={}
	local chaosmap1={}
	for x=0,15 do
		for y=0,15 do
			for z=0,15 do
				chaosmap1[{}]={x,y,z}
			end
		end
	end
	local i=1
	for k,v in pairs(chaosmap1) do
		chaosmap[i]=v
		i=i+1
	end
end
gen_chaosmap()

local nodes,nodeids,groups

local function addnodename(nodenames,nodename)
	local group=string.match(nodename,"^group:(.+)")
	if group then
		if groups[group] then
			for k,v in pairs(groups[group]) do
				nodenames[nodeids[k]]=v
			end
		end
	else
		nodenames[nodeids[nodename]]=true
	end
end

minetest.after(0,function()
	nodes={}
	nodeids={}
	groups={}
	for name,def in pairs(minetest.registered_nodes) do
		local content_id=minetest.get_content_id(name)
		assert(content_id)
		--print("node",name,content_id)
		nodeids[content_id]=name
		nodeids[name]=content_id
		for k,v in pairs(def.groups or {}) do
			if v==true or v>0 then
				groups[k]=groups[k]or {}
				groups[k][name]=true
			end
		end
	end
	for k,v in ipairs({"air","ignore"}) do
		local content_id=minetest.get_content_id(v)
		nodeids[content_id]=v
		nodeids[v]=content_id
	end
	for _,abm in ipairs(abms) do
		local nodenames={}
		local neighbors=abm.neighbors and {} or nil
		for _,nodename in ipairs(abm.nodenames) do
			addnodename(nodenames,nodename)
		end
		if neighbors then
			for _,nodename in ipairs(abm.neighbors) do
				addnodename(neighbors,nodename)
			end
		end
		local abm={
			nodenames=nodenames,
			neighbors=neighbors,
			interval=abm.interval or 1,
			chance=abm.chance or 1,
			catch_up=(abm.catch_up==nil) and true or abm.catch_up,
			action=abm.action
		}
		--print(dump(abm))
		for k,v in pairs(nodenames) do
			nodes[k]=nodes[k] or {abms={}}
			table.insert(nodes[k].abms,abm)
		end
	end
end)

local function pos_to_id(x,y,z)
	return string.format("%s_%s_%s",x,y,z)
end

local npc=#chaosmap

local vmanip,data,data1,data2,ar=nil,{},{},{},nil

local function cmc(bcount,dtime)
	return bcount*dtime
end

local mul=0.5
local function sum(t)
	local n=0
	for k,v in ipairs(t) do
		n=n+v
	end
	return n
end

local function run(func,...)
	func(...)
end

local totaltimes={}

local worldscanner=coroutine.create(function(dtime)
	while true do
		local blocks={}
		local bcount=0
		for _,ref in ipairs(minetest.get_connected_players()) do
			local pos=ref:get_pos()
			local a={x=active_range,y=active_range,z=active_range}
			local p1=vector.subtract(pos,a)
			local p2=vector.add(pos,a)
			local px,py,pz=pos.x,pos.y,pos.z
			for x=floor(p1.x/16),floor(p2.x/16) do
				for y=floor(p1.y/16),floor(p2.y/16) do
					for z=floor(p1.z/16),floor(p2.z/16) do
						local rx,ry,rz=x*16+7.5,y*16+7.5,z*16+7.5
						local dist=((px-rx)^2+(py-ry)^2+(pz-rz)^2)^0.5
						local maxdist=active_range
						if not blocks[pos_to_id(x,y,z)] then
							bcount=bcount+1
							blocks[pos_to_id(x,y,z)]={x,y,z,math.max(0,(1-(dist/maxdist)))}
						else
							local block=blocks[pos_to_id(x,y,z)]
							block[4]=math.max(block[4],(1-(dist/maxdist)))
						end
					end
				end
			end
		end
		for k,v in pairs(blocks) do
			if math.random()>(v[4])^mul then
				blocks[k]=nil
				bcount=bcount-1
			end
		end
		local count=0
		local rcount=0
		local maxcount=cmc(bcount,dtime)
		--print(string.format("processing %s ABMs",bcount))
		local totaltime=0
		local yields=0
		local bactions={}
		for bid,v in pairs(blocks) do
			local starttime=minetest.get_us_time()
			local bx,by,bz=unpack(v)
			local p1,p2={x=bx*16-1,y=by*16-1,z=bz*16-1},{x=bx*16+16,y=by*16+16,z=bz*16+16}
			local vmanip=vmanip or VoxelManip(p1,p2)
			vmanip:read_from_map(p1,p2)
			data,data1,data2=vmanip:get_data(data),vmanip:get_light_data(data1),vmanip:get_param2_data(data2)
			local ap1,ap2=vmanip:get_emerged_area()
			ar=VoxelArea:new{MinEdge=ap1,MaxEdge=ap2}
			for dx=0,15 do for dy=0,15 do for dz=0,15 do
				local x,y,z=bx*16+dx,by*16+dy,bz*16+dz
				local nk=ar:index(x,y,z)
				local i=data[nk]
				if nodes[i] then
					for k,v in ipairs(nodes[i].abms) do
						local matches=false
						if v.interval~=1 then
							--print(abmtimer,abmtimer%v.interval)
						end
						if abmtimer%v.interval==0 and rand(1,v.chance)==1 then
							for cx=-1,1 do
								for cy=-1,1 do
									for cz=-1,1 do
										if cx~=0 or cy~=0 or cz~=0 then
											local ii=data[ar:index(cx+x,cy+y,cz+z)]
											if (not v.neighbors) or v.neighbors[ii] then
													matches=true
													break
											end
										end
									end
									if matches then break end
								end
								if matches then break end
							end
							if matches then
								bactions[bid]=bactions[bid] or {}
								table.insert(bactions[bid],{
									action=function()
										v.action({x=x,y=y,z=z},{name=nodeids[i],param2=data2[nk],param1=data1[nk]})
									end,
									abm=v,
									x=x,y=y,z=z
								})
							end
						end
					end	
				end
			end end end
			count=count+1
			rcount=rcount+1
			totaltime=totaltime+(minetest.get_us_time()-starttime)
			if count>maxcount then
				--print(string.format("%s%% done, %s blocks processed in this step, %s total",math.floor(rcount/bcount*1000)/10,count,rcount))
				dtime=coroutine.yield()
				yields=yields+1
				maxcount=cmc(bcount,dtime)
				count=0
			end
		end
		local stime=minetest.get_us_time()
		for k,act in pairs(bactions) do
			local v=blocks[k]
			local bx,by,bz=unpack(v)
			local p1,p2={x=bx*16-1,y=by*16-1,z=bz*16-1},{x=bx*16+16,y=by*16+16,z=bz*16+16}
			local vmanip=vmanip or VoxelManip(p1,p2)
			vmanip:read_from_map(p1,p2)
			data,data1,data2=vmanip:get_data(data),vmanip:get_light_data(data1),vmanip:get_param2_data(data2)
			local ap1,ap2=vmanip:get_emerged_area()
			ar=VoxelArea:new{MinEdge=ap1,MaxEdge=ap2}
			for k,a in ipairs(act) do
				local v=a.abm
				local matches=false
				local x,y,z=a.x,a.y,a.z
				for cx=-1,1 do
					for cy=-1,1 do
						for cz=-1,1 do
							if cx~=0 or cy~=0 or cz~=0 then
								local ii=data[ar:index(cx+x,cy+y,cz+z)]
								if (not v.neighbors) or v.neighbors[ii] then
										matches=true
										break
								end
							end
						end
						if matches then break end
					end
					if matches then break end
				end
				if matches then
					a.action()
				end
			end
		end
		local cbacktime=minetest.get_us_time()-stime
		print(string.format("processed %s blocks of ABMs, %s blocks to yield, took %s seconds per block, %s seconds total; %s yields; ABM handlers took %s seconds",rcount,maxcount,(totaltime)/1000000/bcount,totaltime/1000000,yields,cbacktime/1000000))
		table.insert(totaltimes,totaltime/1000000)
		while sum(totaltimes) > 5 do
			table.remove(totaltimes,1)
		end
		local avg=sum(totaltimes)/#totaltimes
		if avg > 0.5 then
			mul=mul*1.07
		elseif avg < 0.4 then
			mul=mul*0.95
		end
		abmtimer=abmtimer+1
		dtime=coroutine.yield()
	end
end)
local wait=5
minetest.register_globalstep(function(dtime)
	if wait>0 then
		wait=wait-dtime
		return
	end
	if not nodes then
		return
	end
	local ok,err=coroutine.resume(worldscanner,dtime)
	assert(ok,err)
end)
